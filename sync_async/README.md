# Sync vs Async proof of concept

The purpose of this exercise is to demonstrate the advantages of introducing intra process parallelism

##Concepts:
* threading
* synchronization with Observer pattern(to be revised)
* strategy pattern for scalability of workers

##Abstract:

The use case for this example is a set of tasks that are run until one of them is successful.
That is at the first positive result the execution should terminate and the client should have a response.<br>
All tasks must be run because "a priori" the client doesn't know if any of them will be successful and should 
receive a response whether any task was successful.

##Details:

* ###The project runs a local web server on port 8080 and exposes two api's
  * /go?sync=[true|false]
    * run tasks in synchronous or asynchronous mode according to query parameter "sync" 
  * /go/all
    * runs both synchronous and asynchronous

* ###Both apis return an object with the following parameters:
  * startTime
  * endTime
  * diff(in seconds)
  * isSuccess

* ###Has a Master service interface with two implementations(Sync and Async)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The synchronous service runs workers one by one until one of them is successful<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The asynchronous service runs all the workers simultaneously 
* ###Provides a worker interface with three implementations
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The implementation is simple. Sleeps the thread according to a specific value to mock some heavy-duty work
* ###Tasks are run in an Executor class that exists for the purpose of synchronizing the Master with Workers.(Async only)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Worker threads report to the executor the result of their work.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Master uses the executor to manage all the tasks and return a result.


