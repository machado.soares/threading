package info.soma.threading;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
public class ThreadingApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThreadingApplication.class, args);
	}

}
