package info.soma.threading.services.facade;

import info.soma.threading.services.worker.Worker;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component("MasterSync")
public class MasterSyncImpl implements MasterFacade {

    private final Map<String, Worker> workerMap;

    public MasterSyncImpl(Map<String, Worker> workerMap) {
        this.workerMap = workerMap;
    }

    @Override
    public boolean work() {
        for (Worker worker : workerMap.values()) {
            if (worker.run())
                return true;
        }
        return false;
    }
}
