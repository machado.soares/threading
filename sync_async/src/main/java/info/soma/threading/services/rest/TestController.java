package info.soma.threading.services.rest;

import info.soma.threading.services.facade.MasterFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

@RestController
public class TestController {

    @Autowired
    Map<String, MasterFacade> map;

    @GetMapping("/go")
    public ExecutionResult go(@PathParam("sync") Boolean sync) throws Exception {
        ExecutionResult result = new ExecutionResult();
        result.setStartTime(Instant.now());
        result.setSuccess(sync ? map.get("MasterSync").work() : map.get("MasterAsync").work());
        result.setEndTime(Instant.now());
        result.setDiff();
        return result;
    }

    @GetMapping("/go/all")
    public Map<String, ExecutionResult> go() throws Exception {
        Map<String, ExecutionResult> resultMap = new HashMap<>();
        ExecutionResult async = new ExecutionResult();

        async.setStartTime(Instant.now());
        async.setSuccess(map.get("MasterAsync").work());
        async.setEndTime(Instant.now());
        async.setDiff();
        resultMap.put("async", async);
        ExecutionResult sync = new ExecutionResult();
        sync.setStartTime(Instant.now());
        sync.setSuccess(map.get("MasterSync").work());
        sync.setEndTime(Instant.now());
        sync.setDiff();
        resultMap.put("sync", sync);
        return resultMap;
    }
}
