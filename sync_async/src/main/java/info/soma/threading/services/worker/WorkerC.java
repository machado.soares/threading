package info.soma.threading.services.worker;

import info.soma.threading.services.threadExecutor.Executor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component("workerC")
public class WorkerC implements Worker {

    @Autowired
    private Executor executor;

    @Override
    public boolean run() {
        boolean state = false;
        try {
            Thread.sleep(15000);
            executor.changeState(Thread.currentThread(), state);
        } catch (InterruptedException e) {
            executor.changeState(Thread.currentThread(), false);
            System.out.println("Stacktrace: " + Arrays.toString(e.getStackTrace()));
            return false;
        }
        return state;
    }
}
