package info.soma.threading.services.threadExecutor;

import info.soma.threading.services.worker.Worker;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;


public class Task {

    private String id;
    private Map<String, Worker> workerMap;
    private Map<Thread, Boolean> workerThreads;

    private Task() {
    }

    public Task(Map<String, Worker> workerMap) {
        if (workerMap == null || workerMap.isEmpty()) {
            throw new RuntimeException();
        }
        this.workerMap = workerMap;
        this.workerThreads = new ConcurrentHashMap<>();
    }

    public boolean isFinished() {
        for (Map.Entry<Thread, Boolean> entry : workerThreads.entrySet()) {
            if (entry.getKey().isAlive()) {
                return false;
            }
        }
        return true;
    }

    public void runTask() {
        int counter = 0;
        for (Map.Entry<String, Worker> entry : workerMap.entrySet()) {
            Thread thread = setThread(entry);
            thread.setName("worker-task-test-" + counter++);
            thread.start();
        }
    }

    public void changeState(Thread thread, Boolean value) {
        if (workerThreads.containsKey(thread)) {
            workerThreads.put(thread, value);
        }
    }

    private Thread setThread(Map.Entry<String, Worker> entry) {
        Thread thread = new Thread(() -> entry.getValue().run());
        workerThreads.put(thread, false);
        return thread;
    }

    public Boolean isSuccess() {
        Optional<Map.Entry<Thread, Boolean>> entry = workerThreads.entrySet().stream().filter(Map.Entry::getValue).findFirst();
        return (entry.isPresent());
    }
}
