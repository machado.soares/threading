package info.soma.threading.services.threadExecutor;

import info.soma.threading.services.worker.Worker;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class Executor {


    private final Map<UUID, Task> taskMap = new ConcurrentHashMap<>();

    private final Map<String, Worker> workerMap;

    public Executor(Map<String, Worker> workerMap) {
        this.workerMap = workerMap;
    }

    public Boolean run(UUID uuid) throws Exception {
        Task task = taskMap.getOrDefault(uuid, null);
        if (task != null) {
            task.runTask();
            while (!task.isFinished() && !task.isSuccess()) {
                Thread.sleep(100);
            }
            return task.isSuccess();
        }
        throw new Exception();

    }

    public Executor subscribe(UUID uuid) {
        taskMap.put(uuid, new Task(workerMap));
        return this;
    }

    public void changeState(Thread thread, Boolean value) {
        for (Task task : taskMap.values()) {
            task.changeState(thread, value);
        }
    }


}
