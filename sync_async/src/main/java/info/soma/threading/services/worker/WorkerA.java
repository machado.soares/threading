package info.soma.threading.services.worker;

import info.soma.threading.services.threadExecutor.Executor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("workerA")
public class WorkerA implements Worker {

    @Autowired
    private Executor executor;

    @Override
    public boolean run() {
        boolean state = false;
        try {
            Thread.sleep(1000);
            executor.changeState(Thread.currentThread(), state);
        } catch (InterruptedException e) {
            executor.changeState(Thread.currentThread(), false);
            return false;
        }
        return state;
    }
}
