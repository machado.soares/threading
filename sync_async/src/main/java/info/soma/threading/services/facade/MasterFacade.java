package info.soma.threading.services.facade;

public interface MasterFacade {

    public boolean work() throws Exception;

}
