package info.soma.threading.services.rest;

import java.time.Duration;
import java.time.Instant;

public class ExecutionResult {

    private boolean isSuccess;
    private Instant startTime;
    private Instant endTime;
    private double diff;

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

    public double getDiff() {
        return diff;
    }

    public void setDiff() {
        this.diff = Duration.between(startTime, endTime).toSeconds();
    }
}
