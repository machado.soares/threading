package info.soma.threading.services.worker;

import info.soma.threading.services.threadExecutor.Executor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("workerB")
public class WorkerB implements Worker {
    @Autowired
    private Executor executor;

    @Override
    public boolean run() {
        boolean state = true;
        try {
            Thread.sleep(4000);
            executor.changeState(Thread.currentThread(), state);
        } catch (InterruptedException e) {
            executor.changeState(Thread.currentThread(), false);
            return false;
        }
        return state;
    }
}
