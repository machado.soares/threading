package info.soma.threading.services.worker;

public interface Worker {

    public boolean run();

}
