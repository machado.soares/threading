package info.soma.threading.services.facade;

import info.soma.threading.services.threadExecutor.Executor;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component("MasterAsync")
public class MasterAsyncImpl implements MasterFacade {


    private final Executor executor;

    public MasterAsyncImpl(Executor ex) {
        this.executor = ex;
    }

    @Override
    public boolean work() throws Exception {
        UUID uuid = UUID.randomUUID();
        return executor.subscribe(uuid).run(uuid);
    }
}
